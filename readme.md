# IAM Terraform assignment
A simple definition of an environment with a few users, some groups, policies and bindings.

Note that you will have to manually enable the console access and set access credentials.

# Why didn't I use S3 to store state?
It will be easier for you to run. Best practice is to store state remotely.

# How do I run this?

To run, export your AWS credentials to the environment:

```sh
$ export AWS_ACCESS_KEY_ID="anaccesskey"
$ export AWS_SECRET_ACCESS_KEY="asecretkey"
$ export AWS_DEFAULT_REGION="us-west-2"
```

To see what will be created, run:

```sh
$ terraform plan
```
To create the defined resources, run:
```sh
$ terraform apply
```

