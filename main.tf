provider "aws" {}

##########################
# IAM group definitions  #
##########################
module "groups" {
  source = "./modules/groups"
}

##########################
# IAM user definitions   #
##########################
module "users" {
  source = "./modules/users"
}

##########################
# Put users into groups  #
##########################
module "group_bindings" {
  source = "./modules/group_bindings"

  engineers_group_name = "${module.groups.engineers_group_name}"

  engineer_user_names = [
    "${module.users.bea}",
    "${module.users.alice}",
  ]

  admin_group_name = "${module.groups.admin_group_name}"

  admin_user_names = [
    "${module.users.bofh}"
  ]
}

##########################
# IAM policy definitions #
##########################
module "policies" {
  source = "./modules/policies"
}

##########################
# Bind IAM policies      #
##########################
module "policy_bindings" {
  source = "./modules/policy_bindings"

  admin_group_name    = "${module.groups.admin_group_name}"
  engineer_group_name = "${module.groups.engineers_group_name}"

  admin_policy_arn    = "${module.policies.admin_policy_arn}"
  engineer_policy_arn = "${module.policies.engineer_policy_arn}"
  mfa_policy_arn      = "${module.policies.mfa_policy_arn}"
}