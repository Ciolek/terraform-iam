resource "aws_iam_group_policy_attachment" "admin_policy_binding" {
  group = "${var.admin_group_name}"

  policy_arn = "${var.admin_policy_arn}"
}

resource "aws_iam_group_policy_attachment" "engineer_policy_binding" {
  group = "${var.engineer_group_name}"

  policy_arn = "${var.engineer_policy_arn}"
}

resource "aws_iam_group_policy_attachment" "mfa_admin" {
  group = "${var.admin_group_name}"

  policy_arn = "${var.mfa_policy_arn}"
}

resource "aws_iam_group_policy_attachment" "mfa_engineers" {
  group = "${var.engineer_group_name}"

  policy_arn = "${var.mfa_policy_arn}"
}
