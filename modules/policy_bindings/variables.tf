variable "admin_group_name" {
  description = "The name of the admin group."
  type        = string
}

variable "engineer_group_name" {
  description = "The name of the engineer group."
  type        = string
}

variable "admin_policy_arn" {
  description = "The arn of the admin policy."
  type        = string
}

variable "engineer_policy_arn" {
  description = "The arn of the engineer policy."
  type        = string
}

variable "mfa_policy_arn" {
  description = "The arn of the MFA policy."
  type        = string
}
