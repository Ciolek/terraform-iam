resource "aws_iam_policy" "admin_policy" {
  name = "admin_policy"

  policy = file("${path.module}/templates/admin_policy.json")
}

resource "aws_iam_policy" "engineer_policy" {
  name = "engineer_policy"

  policy = file("${path.module}/templates/engineer_policy.json")
}

resource "aws_iam_policy" "mfa_policy" {
  name = "mfa_policy"

  policy = file("${path.module}/templates/mfa_policy.json")
}

