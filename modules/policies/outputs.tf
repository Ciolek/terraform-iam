output "admin_policy_arn" {
  value       = "${aws_iam_policy.admin_policy.arn}"
  description = "Admin policy arn"
}

output "engineer_policy_arn" {
  value = "${aws_iam_policy.engineer_policy.arn}"
}

output "mfa_policy_arn" {
  value = "${aws_iam_policy.mfa_policy.arn}"
}
