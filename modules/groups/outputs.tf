output "admin_group_id" {
  value       = aws_iam_group.admins.id
  description = "This id of the admins group."
}

output "admin_group_name" {
  value       = aws_iam_group.admins.name
  description = "The name of the admins group."
}

output "engineers_group_id" {
  value       = aws_iam_group.engineers.id
  description = "The id of the engineers group."
}

output "engineers_group_name" {
  value       = aws_iam_group.engineers.name
  description = "The name of the engineers group."
}

