variable "admin_group_name" {
  description = "The name of the admin group."
  type        = string
}

variable "engineers_group_name" {
  description = "The name of the engineers group."
  type        = string
}

variable "engineer_user_names" {
  description = "A list of users to bind to the engineer group."
  type        = list(string)
}

variable "admin_user_names" {
  description = "A list of users to bind to the admin group."
  type        = list(string)
}
