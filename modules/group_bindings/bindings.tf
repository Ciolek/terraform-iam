resource "aws_iam_group_membership" "bofh_admins" {
  name = "bofh-admin-membership"

  users = "${var.admin_user_names}"
  group = "${var.admin_group_name}"
}

resource "aws_iam_group_membership" "engineers_membership" {
  name = "engineers-membership"

  users = "${var.engineer_user_names}"
  group = "${var.engineers_group_name}"
}
