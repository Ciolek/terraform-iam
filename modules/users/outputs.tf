output "bofh" {
  value       = aws_iam_user.bofh.name
  description = "User: bofh"
}

output "alice" {
  value       = aws_iam_user.alice.name
  description = "User: alice."
}

output "bea" {
  value       = aws_iam_user.bea.name
  description = "User: bea"
}
